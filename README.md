# Real Time Chat App Using Websockets

![alt text](http://i.piccy.info/i9/07ee03ff2ac9fbbf5db9999889897cb1/1538768638/13975/1273571/Screenshot_5.png)


```
$ git clone https://gitlab.com/andrew.velkov/chat.git
```

```
$ cd chat 
```

```
$ yarn install
```

```
$ yarn start
```
```
$ node server
```

Visit `http://localhost:3003/` 