const http = require('http');
const static = require('node-static');
const WebSocket = require('ws');

const file = new static.Server('./build', { cache: 3600 });

const port = 8080;

http.createServer(function (request, response) {
  request.addListener('end', function () {
    file.serve(request, response, function (e, res) {
        if (e && (e.status === 404)) {
          file.serveFile('./not-found.html', 404, {}, request, response);
        }
      });
  }).resume();
}).listen(port, () => {
  console.log(`Server running on port: ${ port }`);
});

const wss = new WebSocket.Server({ port: 3000 })
const messages = [];

wss.on('connection', ws => {
  ws.on('message', message => {
    const newMessage = { id: messages.length, ...JSON.parse(message) }

    messages.push(newMessage);

    wss.clients.forEach(client => {
      client.readyState === WebSocket.OPEN && client.send(JSON.stringify({ type: 'message', messages: newMessage }));
    });

    console.log('message:', newMessage);
  });

  ws.send(JSON.stringify({
    type: 'messages',
    messages,
  }));

  console.log('Connected!');
});