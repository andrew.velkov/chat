import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { animateScroll } from 'react-scroll';

import Input from 'components/Form/Input';
import MessagesList from 'components/Messages/List';

import css from 'style/pages/Main';

export default class Main extends Component {
  static propTypes = {
    ws: PropTypes.any,
    ring: PropTypes.any,
  }

  constructor(props) {
    super(props);

    this.state = {
      nickname: '',
      message: '',
      data: [],
      status: '',
      error: '',
    };
  }

  componentWillMount() {
    const { ws } = this.props;

    ws.onopen = () => this.setStatus('Online');
    ws.onclose = () => this.setStatus('Disconnected');
    ws.onmessage = response => this.printMessage(response.data);
  }

  setStatus = (value) => {
    this.setState({
      status: value,
    });
  }

  printMessage = (dataType) => {
    const dataTypes = JSON.parse(dataType);
    const { status } = this.state;

    if (dataTypes.type === 'messages' && status === 'Online') {
      this.setState({ data: dataTypes.messages.slice(-10) });
    }

    if (dataTypes.type === 'message') {
      this.setState((prevState) => ({
        data: [...prevState.data, dataTypes.messages],
      }));
      this.props.ring.play();

      animateScroll.scrollToBottom();
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleKeyPress = (e) => {
    const { ws } = this.props;
    const { nickname, message } = this.state;
    const dataPost = { nickname, message };
    const isValid = nickname === '' || message === '';

    if (e.key === 'Enter') {
      if (!isValid) {
        ws.send(JSON.stringify(dataPost));

        this.setState({
          nickname: '',
          message: '',
          error: '',
        });
      } else {
        this.setState({
          error: {
            nickname: nickname === '' && 'Nickname is required',
            message: message === '' && 'Message is required',
          },
        });
      }
    }
  }

  render() {
    const { nickname, message, data, status, error } = this.state;

    return (
      <section className={ css.wrap }>
        <div className={ cx(css.wrap__container, css.wrap__container_reverse) }>
          <div className={ css.wrap__item }>
            <h3 className={ cx(css.wrap__title, css.wrap__title_status) }>
              Add message | status: <span>{ status }</span>
            </h3>

            <form className={ css.formGroup }>
              <div className={ css.formGroup__item }>
                <Input
                  label='Nickname'
                  name='nickname'
                  value={ nickname }
                  errorText={ error.nickname }
                  onChange={ (e) => this.handleChange(e) }
                  onKeyPress={ (e) => this.handleKeyPress(e) }
                />
              </div>
              <div className={ css.formGroup__item }>
                <Input
                  label='Message'
                  name='message'
                  // multiLine={ true }
                  value={ message }
                  errorText={ error.message }
                  onChange={ (e) => this.handleChange(e) }
                  onKeyPress={ (e) => this.handleKeyPress(e) }
                />
              </div>
            </form>

          </div>
          <div className={ cx(css.wrap__item, css.wrap__item_transparent) }>
            <h3>Messages</h3>
            <ul className={ css.messages }>
              {data.length > 0 && data.map(item => {
                return (
                  <MessagesList key={ item.id } data={ item } />
                );
              })}

              {data.length === 0 && <li className={ css.wrap__notification }>No messages</li>}
            </ul>
          </div>
        </div>
      </section>
    );
  }
}
