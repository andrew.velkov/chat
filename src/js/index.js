import React from 'react';
import ReactDOM from 'react-dom';

import Routes from 'routes';

import 'style/main';

// Render it to DOM
ReactDOM.render(
  <Routes />,
  document.getElementById('root')
);
