import React from 'react';
import PropTypes from 'prop-types';

import css from 'style/pages/Main';

const List = ({ data }) => (
  <li className={ css.messages__item }>
    <div className={ css.messages__body }>
      <h5 className={ css.messages__title }>
        { data.nickname }
      </h5>
      <p className={ css.messages__info }>
        { data.message }
      </p>
    </div>
  </li>
);

List.propTypes = {
  data: PropTypes.any,
};

export default List;
