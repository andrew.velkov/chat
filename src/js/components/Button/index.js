import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { RaisedButton, FlatButton, IconButton, FloatingActionButton } from 'material-ui';

import css from 'style/components/Button';

const Button = ({
  typeButton = 'raise',
  iconName,
  className,
  onClick,
  children,
  ...button
}) => (
  <span>
    {typeButton === 'raise' &&
      <RaisedButton className={ className } label={ children } onClick={ onClick } { ...button } />
    }

    {typeButton === 'flat' &&
      <FlatButton className={ className } label={ children } onClick={ onClick } { ...button } />
    }

    {typeButton === 'icon' &&
      <IconButton className={ className } onClick={ onClick }>
        {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
      </IconButton>
    }

    {typeButton === 'circle' &&
      <FloatingActionButton onClick={ onClick }>
        {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
      </FloatingActionButton>
    }
  </span>
);

Button.propTypes = {
  typeButton: PropTypes.string,
  iconName: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
};

export default Button;
