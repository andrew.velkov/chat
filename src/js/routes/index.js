import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from 'routes/history';
import App from 'containers/App';

import Main from 'pages/Main';
import NotFound from 'pages/NotFound';

const host = window.location.hostname;
const ws = new WebSocket(`ws://${ host }:3000`);
const ring = new Audio('//todo-list.ho.ua/_mp3/sound.mp3');

export default class Routes extends Component {
  render() {
    return (
      <Router history={ history }>
        <App>
          <Switch>
            <Route exact path='/' render={ () => <Main ws={ ws } ring={ ring } /> } />
            <Route component={ NotFound } />
          </Switch>
        </App>
      </Router>
    );
  }
}
